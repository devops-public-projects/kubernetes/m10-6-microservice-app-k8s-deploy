
# K8s Microservice App Best Practices
Improve a Microservices application in Kubernetes with Production & Security Best Practices.

## Technologies Used
- Kubernetes
- Redis
- Linux
- Linode LKE

## Project Description
- Create K8s manifests for Deployments and Services for all microservices of an online shop application
- Deploy microservices to Linode’s managed Kubernetes cluster

## Prerequisites
- [Previous M10-5 Project Completed](https://gitlab.com/devops-public-projects/kubernetes/m10-5-prometheus-monitoring-k8s-deploy)

## Guide Steps
### 1) Version Tag All Container Images
We already completed this previously as our project is running **v0.8.0**

### 2) Adding Liveness Probe to our Config
**Note:** This are container attribute to make sure the App inside the Pod is OK.

We will add a **livenessProbe** to each **Deployment** in our `config.yaml`. Below is an example for the **emailservice** **Deplyoment**. We add a probe every 5 seconds to port 8080 for the service to make sure it's working.

```yaml
livenessProbe:
 grpc:
  port: 8080
 periodSeconds: 5
```

- Add this to every **Deployment** (11 in total), changing the `port` as needed for the specific service.

### 3) Adding a Readiness Probe to our Config
**Note:** This is a container attribute to make sure the App starts up successfully and is ready to receive traffic.

We will need to add this to every **Deployment** container section as well. Below is an example for the **emailservice**. 

- We will use the **grpc** protocol for all services except Frontend and Redis
```yaml
readinessProbe:
 grpc:
  port: 8080
 periodSeconds: 5
```

- **Redis** will use **tcpSocket**
```yaml
readinessProbe:
 initialDelaySeconds: 5
 tcpSocket:
  port: 6379
 periodSeconds: 5
```

- **Frontend** will use **httpGet**
```yaml
readinessProbe:
 httpGet:
  path: "/_healthz"
  port: 8080
 periodSeconds: 5
```
### 4) Add Resource Requests
**Note:** This is a container attribute

We will add resource requests for **emailservice**, **adservice**, and **redis**

```yaml
#Redis example
resources:
 requests:
  cpu: 70m
  memory: 200Mi
```

### 5) Add Resource Limits
**Note:** This is a container attribute

We will add resource requests for **emailservice**, **adservice**, and **redis**

```yaml
#Redis Example
resources:
 limits:
  cpu: 125m
  memory: 300Mi
```

### 6) Switch from NodePort to LoadBalancer Service for frontend
```yaml
spec:
 type: LoadBalancer #Was NodePort
 selector:
  app: frontend
 ports:
 - protocol: TCP
   port: 80
   targetPort: 8080
 # nodePort: 30007 #Line Removed
```

### 7) Add Replica Amount
- We will modify the replica count from 1 to **2** for all **Deployments**